<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    //
    public function getLogin(){
        return view('admin.login');
    }
    public function postLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            // dd($credentials);
            return redirect('admin/ad-min');
        }
        else 

        return redirect()->back()->with(['error'=>' email khoặc mật khẩu không đúng ']);

        return view('admin.login');

    }

    public function getLogout(Request $request)
    {
        Auth::logout();

        return view('admin.login');
    }
}
