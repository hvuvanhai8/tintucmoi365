<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class New_details extends Model
{
    //
    protected $table = 'new_details';

    protected $fillable = [
        'new_id', 'new_title','new_slug','new_describe','new_content','new_image','new_hot','new_view','new_kind',
    ];
}
