<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class New_kind extends Model
{
    //
    protected $table = 'new_kind';

    protected $fillable = [
        'kin_id', 'kind_cat','kind_name','kind_slug',
    ];
}
