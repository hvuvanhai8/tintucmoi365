<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Admin
 Route::get('/login', 'Admin\LoginController@getLogin');
 Route::post('/postlogin', 'Admin\LoginController@postLogin')->name('admin.post.login');

 Route::get('/ad-min', function () {
    return view('admin.index');
});